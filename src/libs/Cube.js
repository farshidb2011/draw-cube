import Point from './Point'

/**
 * calculate draw cube with 3 point
 * 1. start point
 * 2. end point
 * 3. deep point
 */
export default class Cube {
  constructor(startPoint, endPoint, deepPoint) {
    this.diff = new Point(deepPoint.x - endPoint.x, deepPoint.y - endPoint.y);
    this.mainRect = null;
    this.lines = null;
    this.otherHand = null;

    this._init(startPoint, endPoint, deepPoint);
  }

  // calculate with rectangle
  get width() {
    return this.mainRect[3].x - this.mainRect[0].x;
  }
  
  // calculate with rectangle
  get height() {
    return this.mainRect[3].y - this.mainRect[0].y;
  }

  // daterimne other point of rectangle
  _init(startPoint, endPoint, deepPoint) {
    this.mainRect = [
      startPoint,
      new Point(endPoint.x, startPoint.y),
      new Point(startPoint.x, endPoint.y),
      endPoint,
    ];
    this._generateLines();
    this._generateOtherHand();
  }

  // according 4 point of main rect calculate lines
  _generateLines() {
    this.lines = this.mainRect.map((item) => [
      item,
      new Point(item.x + this.diff.x, item.y + this.diff.y),
    ]);
  }

  // inital config for other side of cube
  _generateOtherHand() {
    this.otherHand = {
      ...this.lines[0][1],
      width: this.width,
      height: this.height,
      stroke: 'black',
      strokeWidth: 4
    }
  }

  // remove lines and other side
  reset(){
    this.lines = null;
    this.otherHand = null;
  }
}