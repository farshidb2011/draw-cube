/**
 * Class coordinate point on screen
 */
export default class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}